const isDev = process.env.NODE_ENV !== 'production'

export default {
  // Зададим переменные
  env: {
    // baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },

  server: {
    host: '0.0.0.0' // слушаем всё
  },

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Grader24',
    htmlAttrs: {
      lang: 'ru'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  rootDir: __dirname,

  serverMiddleware: [
  ],

  router: {
    prefetchLinks: false
  },

  loading: { color: 'red' },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  'normalize.css',
  './assets/scss/style.scss',
  // './assets/scss/global-styles.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  '@nuxtjs/axios',
  'nuxt-trailingslash-module',
  'nuxt-webfontloader',
  'cookie-universal-nuxt',
  '@nuxtjs/style-resources'
  ],

  webfontloader: {
    events: false,
    google: {
      families: ['Montserrat:400,500,600:cyrillic&display=swap']
    },
    timeout: 5000
  },

  styleResources: {
      // your settings here
      // scss: ['./assets/scss/style.scss'], // alternative: scss
      less: [],
      stylus: []
    },

  /*
  ** Axios module configuration
  */
  axios: {
  // See https://github.com/nuxt-community/axios-module#options
  },

  render: {
  // http2: {
  //     push: true,
  //     pushAssets: (req, res, publicPath, preloadFiles) => preloadFiles
  //     .map(f => `<${publicPath}${f.file}>; rel=preload; as=${f.asType}`)
  //   },
  // compressor: false,
  resourceHints: false,
  etag: false,
  static: {
    etag: false
  }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build

  build: {

    optimizeCss: false,

    filenames: {
      app: ({ isDev }) => isDev ? '[name].js' : 'js/[contenthash].js',
      chunk: ({ isDev }) => isDev ? '[name].js' : 'js/[contenthash].js',
      css: ({ isDev }) => isDev ? '[name].css' : 'css/[contenthash].css',
      img: ({ isDev }) => isDev ? '[path][name].[ext]' : 'img/[contenthash:7].[ext]',
      font: ({ isDev }) => isDev ? '[path][name].[ext]' : 'fonts/[contenthash:7].[ext]',
      video: ({ isDev }) => isDev ? '[path][name].[ext]' : 'videos/[contenthash:7].[ext]'
    },

    ...(!isDev && {
      html: {
        minify: {
          collapseBooleanAttributes: true,
          decodeEntities: true,
          minifyCSS: true,
          minifyJS: true,
          processConditionalComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          trimCustomFragments: true,
          useShortDoctype: true
        }
      }
    }),

    splitChunks: {
      layouts: true,
      pages: true,
      commons: true
    },

    optimization: {
      minimize: !isDev
    },

    ...(!isDev && {
      extractCSS: {
        ignoreOrder: true
      }
    }),

    transpile: ['vue-lazy-hydration', 'intersection-observer'],

    // postcss: {
    //   plugins: {
    //     ...(!isDev && {
    //       cssnano: {
    //         preset: ['advanced', {
    //           autoprefixer: false,
    //           cssDeclarationSorter: false,
    //           zindex: false,
    //           discardComments: {
    //             removeAll: true
    //           }
    //         }]
    //       }
    //     })
    //   },

    //   ...(!isDev && {
    //     preset: {
    //       browsers: 'cover 99.5%',
    //       autoprefixer: true
    //     }
    //   }),

    //   order: 'cssnanoLast'
    // },
  }
}

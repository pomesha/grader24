module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '3e8b5ba7cc38cf46138df3d74f77b325'),
  },
});

# Проект Grader24.ru

Docker окружение включает в себя:  
а) node:14-alpine  
б) nginx:alpine  

## Getting started  

1. Установка необходимого приложения, для этого нужно запустить отдельно "service: node", из состава docker-compose.yml, в интерактивном режиме:

	$ docker  run -it --rm \
	-u 1000:1000 \
	-w /home/node \
	-v $(pwd):/home/node \
	-p 1337:1337 -p 3000:3000 \
	node:14-alpine \
	/bin/sh  

	Примечание:  
	- используем -user 1000:1000, для того чтобы при создании из контейнера Docker проекта наследовались права на запись из хоста, можно редактировать файлы на хосте.  

	$ cd backend && yarn  
	$ cd ../frontend && yarn

2. Устанавливаем frontend:  
	- yarn create nuxt-app <project-name> - в моём случае: "frontend"  
	- HOST=0.0.0.0 yarn dev. Или редактируем nuxt.config.js, добавляя:  
	```
	// настройка сервера
	export default {
	  server: {
	    host: '0.0.0.0' // слушаем всё
	  }
	},

	```

  


## Дополнения

смотреть логи контейнера после запуска docker-compose:  
$ docker-compose logs -f <name_container> 

```
cd existing_repo
git remote add origin https://gitlab.com/pomesha/nginx-nodejs-database.git
git branch -M main
git push -uf origin main
```
